package com.twuc.webApp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Product {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 1, max = 64)
    private String name;

    @NotNull
    @Min(value = 1)
    @Max(value = 10000)
    private Integer price;

    @NotNull
    @Size(min = 1, max = 32)
    private String unit;
    public Product() {
    }

    public Product(String name, Integer price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}